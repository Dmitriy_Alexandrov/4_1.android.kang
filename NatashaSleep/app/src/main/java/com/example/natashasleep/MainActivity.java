package com.example.natashasleep;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void ballClick(View view) {
        text = findViewById(R.id.textV);
        Random rand = new Random();

        String phrases[] = {"фраза1", "фраза2", "фраза3"};
        text.setText(phrases[Math.abs(rand.nextInt()%3)]);

    }
}