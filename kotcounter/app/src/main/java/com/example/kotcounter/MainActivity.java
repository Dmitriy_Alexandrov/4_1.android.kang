package com.example.kotcounter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button helloButton;
    private TextView helloText;
    private Button crowButton;
    private int counter = 0;
    private int cats = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        helloText = findViewById(R.id.textView);
        helloButton = findViewById(R.id.button);
        crowButton = findViewById(R.id.crowbutton);

        helloButton.setOnClickListener(v -> {
            helloText.setText("Hello");
        });

        crowButton.setOnClickListener(v -> {
            helloText.setText("Насчитали " + counter++ + " ворон");
        });
    }

    public void onClick(View view) {
        helloText.setText("Насчитали " + cats++ + " кошек");
    }
}