package com.example.trafficlight;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    private Button redButton;
    private Button greenButton;
    private Button yellowButton;
    private TextView text;
    private ConstraintLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        redButton = findViewById(R.id.red_button);
        greenButton = findViewById(R.id.gre_button);
        yellowButton = findViewById(R.id.yel_button);
        text = findViewById(R.id.textView);
        layout = findViewById(R.id.layyout);

        redButton.setOnClickListener(v -> {
            text.setText(R.string.red_string);
            layout.setBackgroundColor(Color.RED);
        });
        greenButton.setOnClickListener(v -> {
            text.setText(R.string.green_string);
            layout.setBackgroundColor(Color.GREEN);
        });
        yellowButton.setOnClickListener(v -> {
            text.setText(R.string.yellow_string);
            layout.setBackgroundColor(Color.YELLOW);
        });
    }
}