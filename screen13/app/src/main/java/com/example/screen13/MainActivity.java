package com.example.screen13;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private float mBackLightValue = 0.5f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(Settings.ACTION_DISPLAY_SETTINGS);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void onClick(View v) {
        // Получим Display из the WindowManager
        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE))
                .getDefaultDisplay();

        // Теперь получим необходимую информацию
        String width = Integer.toString(display.getWidth());
        String height = Integer.toString(display.getHeight());
        String orientation = Integer.toString(display.getOrientation());
        String info = "Ширина: " + width + "; Высота: " + height
                + "; Ориентация: " + orientation;

        TextView infoTextView = (TextView) findViewById(R.id.textViewInfo);
        infoTextView.setText(info);

        TextView infoTextView2 = findViewById(R.id.textView2);

        try {
            int curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
            infoTextView2.setText("Текущая яркость экрана: " + curBrightnessValue);
        } catch (Settings.SettingNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }




}