package com.example.preference25;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private int cnt = 0;
    private TextView voronCnt;
    private Button voronBtn;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        prefs = getSharedPreferences("settings", Context.MODE_PRIVATE);

        voronCnt = findViewById(R.id.voron_cnt);
        voronBtn = findViewById(R.id.button);
    }

    public void onClick(View view) {
        cnt++;
        voronCnt.setText("Ворон насчитано " + cnt);
    }
    @Override
    public void onPause() {
        super.onPause();

        // Запоминаем данные
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("counter", cnt).apply();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (prefs.contains("counter")) {
            // Получаем число из настроек
            cnt = prefs.getInt("counter", 0);
            // Выводим на экран данные из настроек
            voronCnt.setText("Ворон насчитано " + cnt);
        }
    }
}