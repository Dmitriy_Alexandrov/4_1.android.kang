package com.example.notification14;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.Person;

public class MainActivity extends AppCompatActivity {

    // Идентификатор уведомления
    private static final int NOTIFY_ID = 101;

    // Идентификатор канала
    private static String CHANNEL_ID = "Cat channel";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void onClick(View v) {
//        Intent notificationIntent = new Intent(MainActivity.this, MainActivity.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(MainActivity.this,
//                0, notificationIntent,
//                PendingIntent.FLAG_CANCEL_CURRENT);
//
//        NotificationCompat.Builder builder =
//                new NotificationCompat.Builder(MainActivity.this, CHANNEL_ID)
//                        .setSmallIcon(R.drawable.ic_launcher_background)
//                        .setContentTitle("Напоминание")
//                        .setContentText("Пора покормить кота")
//                        .setColor(Color.GREEN)
//                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                        .setContentIntent(contentIntent);
//
//        NotificationManagerCompat notificationManager =
//                NotificationManagerCompat.from(MainActivity.this);
//        notificationManager.notify(NOTIFY_ID, builder.build());

        Intent notificationIntent = new Intent(MainActivity.this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        Person murzik = new Person.Builder().setName("Мурзик").build();
        Person vaska = new Person.Builder().setName("Васька").build();

        NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle
                (murzik)
                .setConversationTitle("Android chat")
                .addMessage("Привет котаны!", System.currentTimeMillis(), murzik)
                .addMessage("А вы знали, что chat по-французски кошка?", System
                                .currentTimeMillis(),
                        murzik)
                .addMessage("Круто!", System.currentTimeMillis(),
                        vaska)
                .addMessage("Ми-ми-ми", System.currentTimeMillis(), vaska)
                .addMessage("Мурзик, откуда ты знаешь французский?", System.currentTimeMillis(),
                        vaska)
                .addMessage("Шерше ля фам, т.е. ищите кошечку!", System.currentTimeMillis(),
                        murzik);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(MainActivity.this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setContentIntent(pendingIntent)
                        .addAction(R.drawable.ic_launcher_background, "Запустить активность",
                                pendingIntent)
                        .setStyle(messagingStyle)
                        .setAutoCancel(true); // автоматически закрыть уведомление после нажатия

        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(MainActivity.this);
        notificationManager.notify(NOTIFY_ID, builder.build());

    }

    public void deleteNot(View view) {
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(MainActivity.this);
        // Удаляем все свои уведомления
        notificationManager.cancelAll();
    }
}