package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    //private lateinit var mHelloTextView: TextView;
    private TextView mHelloTextView;
    private ImageButton imgButt;
    private boolean isPressed = true;
    private EditText plainText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHelloTextView = findViewById(R.id.textView);
        plainText = findViewById(R.id.catName);
        imgButt = findViewById(R.id.imageButton);
        imgButt.setOnClickListener(v -> {
            isPressed = !isPressed;
            if (isPressed) {
                mHelloTextView.setText("Привет!" + plainText.getText());
            } else {
                mHelloTextView.setText("Бонифаций"+ plainText.getText());
            }

        });
    }
}