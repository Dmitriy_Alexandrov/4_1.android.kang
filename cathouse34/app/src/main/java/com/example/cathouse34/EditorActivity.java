package com.example.cathouse34;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.cathouse34.data.HotelContract;
import com.example.cathouse34.data.HotelDbHelper;

public class EditorActivity extends AppCompatActivity {

    private EditText mNameEditText;
    private EditText mCityEditText;
    private EditText mAgeEditText;
    private Spinner mGenderSpinner;
    private HotelDbHelper mDbHelper;

    /**
     * Пол для гостя. Возможные варианты:
     * 0 для кошки, 1 для кота, 2 - не определен.
     */
    private int mGender = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        mNameEditText = (EditText) findViewById(R.id.edit_guest_name);
        mCityEditText = (EditText) findViewById(R.id.edit_guest_city);
        mAgeEditText = (EditText) findViewById(R.id.edit_guest_age);
        mGenderSpinner = (Spinner) findViewById(R.id.spinner_gender);

        setupSpinner();
        mDbHelper = new HotelDbHelper(this);
    }

    /**
     * Настраиваем spinner для выбора пола у гостя.
     */
    private void setupSpinner() {

        ArrayAdapter genderSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_gender_options, android.R.layout.simple_spinner_item);

        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        mGenderSpinner.setAdapter(genderSpinnerAdapter);
        mGenderSpinner.setSelection(2);

        mGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.gender_female))) {
                        //mGender = HotelContract.GuestEntry.GENDER_FEMALE; // Кошка
                        mGender = 0; // Кошка
                    } else if (selection.equals(getString(R.string.gender_male))) {
                        //mGender = HotelContract.GuestEntry.GENDER_MALE; // Кот
                        mGender = 1; // Кот
                    } else {
                        //mGender = HotelContract.GuestEntry.GENDER_UNKNOWN; // Не определен
                        mGender = 2; // Не определен
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mGender = 2; // Unknown
            }
        });
    }

    public void onClick(View view) {
        insertGuest();
    }

    public void insertGuest() {
        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Создаем объект ContentValues, где имена столбцов ключи,
        // а информация о госте является значениями ключей
        ContentValues values = new ContentValues();
        values.put(HotelContract.GuestEntry.COLUMN_NAME, mNameEditText.getText().toString());
        values.put(HotelContract.GuestEntry.COLUMN_CITY, mCityEditText.getText().toString());
        values.put(HotelContract.GuestEntry.COLUMN_GENDER, mGender);
        values.put(HotelContract.GuestEntry.COLUMN_AGE, mAgeEditText.getText().toString());
        long newRowId = db.insert(HotelContract.GuestEntry.TABLE_NAME, null, values);
    }
}