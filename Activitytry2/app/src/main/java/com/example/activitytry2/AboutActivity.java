package com.example.activitytry2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class AboutActivity extends Activity {
    public final static String from = "com.example.activitytry2.AboutActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        String res = getIntent().getStringExtra("komu");
        String desc = getIntent().getStringExtra("desc");
        TextView infoTextView = (TextView)findViewById(R.id.textView_about_content);
        infoTextView.setText("вам передали " + res +  " c описанием " + desc + " от " + getIntent().getStringExtra("from"));
//        try {
//            Thread.sleep(5000);
//        } catch (Exception e) {
//
//        }
        Intent answarIntent = new Intent();
        answarIntent.putExtra(from, "активити");
        setResult(RESULT_OK, answarIntent);
        //finish();
    }
}
