package com.example.activitytry2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public final static String from = "com.example.activitytry2.MainActivity";
    private TextView text;
    private TextView text2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        text = findViewById(R.id.komu);
        text2 = findViewById(R.id.opisanie);
        Intent intent = new Intent(MainActivity.this, AboutActivity.class);
        intent.putExtra("komu", text.getText().toString());
        intent.putExtra("desc", text2.getText().toString());
        intent.putExtra("from", from);

        startActivityForResult(intent, 1);
    }

    public void birthday(View view) {
        Intent intent = new Intent(MainActivity.this, BirthdayActivity.class);
        startActivityForResult(intent, 2);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        TextView infoTextView = (TextView) findViewById(R.id.textView);

        if (true) {
            if (requestCode == 1) {
                String res = data.getStringExtra("com.example.activitytry2.AboutActivity");
                infoTextView.setText(res);
            }else if (requestCode == 2) {
                String res = data.getStringExtra("com.example.activitytry2.BirthsdayActivity");
                infoTextView.setText(res);
            } else {
                infoTextView.setText("ошибочка");
            }
        }
    }
}