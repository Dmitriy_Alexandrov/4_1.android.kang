package com.example.menu11;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        TextView textView = findViewById(R.id.textView);
        if (item.getItemId() == R.id.action_cat1) {
            textView.setText("Вы выбрали кота!");
            return true;
        } else if (item.getItemId() == R.id.action_cat2) {
            textView.setText("Вы выбрали кошку!");
            return true;
        } else if (item.getItemId() == R.id.action_cat3) {
            textView.setText("Вы выбрали котёнка!");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}