package com.example.lifecyclekot22;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.println("MainActivity onCreate() called");
    }
    @Override
    public void onStart() {
        super.onStart();
        System.out.println("MainActivity onStart() called");
    }

    @Override
    public void onRestart() {
        super.onRestart();
        System.out.println("MainActivity onRestart() called");
    }
    @Override
    public void onResume() {
        super.onResume();
        System.out.println("MainActivity onResume() called");
    }
    @Override
    public void onPause() {
        super.onPause();
        System.out.println("MainActivity onPause() called");
    }
    @Override
    public void onStop() {
        super.onStop();
        System.out.println("MainActivity onStop() called");
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("MainActivity onDestroy() called");
    }
}