package com.example.toast9;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    public void printToast(View view) {
        //Toast toast = Toast.makeText(getApplicationContext(), "Пора покормить кота!", Toast.LENGTH_LONG);
        //toast.setGravity(Gravity.TOP, 0, 0);
        //toast.show();

        Toast toast2 = Toast.makeText(getApplicationContext(), "Пора покормить кота!", Toast.LENGTH_LONG);
        toast2.setGravity(Gravity.CENTER, 0, 0);
        LinearLayout toastContainer = (LinearLayout) toast2.getView();
        ImageView catImageView = new ImageView(getApplicationContext());
        catImageView.setImageResource(R.mipmap.whiskas);
        toastContainer.addView(catImageView, 0);
        toast2.show();
    }
}